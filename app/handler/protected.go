package handler

import (
	"net/http"

	"gitlab.com/mobica-workshops/examples/go/gorilla/book-bff/app/model"
)

func (c Controller) ProtectedEndpoint(w http.ResponseWriter, r *http.Request) {
	//ctx := r.Context()
	//c.Logger.Printf("Context uid: %v", ctx.Value("uid").(string))
	//c.Logger.Printf("Context employee: %v", ctx.Value("employee").(model.Employee))
	ResponseJSON(w, model.Response{Status: 200, Message: "Protected"})
}
