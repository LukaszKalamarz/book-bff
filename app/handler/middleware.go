package handler

import (
	"fmt"
	"net/http"

	"github.com/dgrijalva/jwt-go"

	"gitlab.com/mobica-workshops/examples/go/gorilla/book-bff/app/model"
)

// JSONContentTypeMiddleware will add the json content type header for all endpoints
func JSONContentTypeMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("content-type", "application/json; charset=UTF-8")
		next.ServeHTTP(w, r)
	})
}

func (c Controller) TokenVerifyMiddleWare(authorizedOnly bool, next http.HandlerFunc) http.HandlerFunc {
	var errModel model.Error
	return func(w http.ResponseWriter, r *http.Request) {
		if authorizedOnly || r.Method != "GET" {
			var errorObject model.Error
			authToken, err := parseAuthorizationHeader(r)
			if err != nil {
				errModel.Message = err.Error()
				RespondWithError(w, http.StatusBadRequest, errModel)
				return
			}

			token, err := jwt.Parse(authToken, func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was error")
				}

				claims, ok := token.Claims.(jwt.MapClaims)
				claimErr := claims.Valid()
				if ok && claimErr == nil {
					return []byte(c.Config.Server.JwtSecret), nil
				} else {
					return nil, claimErr
				}

			})

			if err != nil {
				errorObject.Message = err.Error()
				RespondWithError(w, http.StatusUnauthorized, errorObject)
				return
			}

			if token.Valid {
				next.ServeHTTP(w, r)
			} else {
				errorObject.Message = err.Error()
				RespondWithError(w, http.StatusUnauthorized, errorObject)
				return
			}
		} else {
			next.ServeHTTP(w, r)
		}
	}
}
