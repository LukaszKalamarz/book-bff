package model

type ApiHealth struct {
	BookList  bool `json:"bookList"`
	BookAdmin bool `json:"bookAdmin"`
}

type Health struct {
	Alive     bool `json:"alive"`
	Redis     bool `json:"redis"`
	ApiHealth `json:"servicesHealth"`
}

type CheckApiHealthContent struct {
	Alive bool `json:"alive"`
}

type CheckApiHealth struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Content CheckApiHealthContent
}
