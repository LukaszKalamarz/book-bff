package model

import (
	"bytes"

	"golang.org/x/crypto/bcrypt"
)

type User struct {
	Email             string `json:"email"`
	Password          string `json:"password"`
	ReturnSecureToken bool   `json:"returnSecureToken"`
}

type BookAdminObject struct {
	ID           string `json:"id"`
	Email        string `json:"email"`
	FirstName    string `json:"firstName"`
	LastName     string `json:"lastName"`
	PasswordHash string `json:"passwordHash"`
	Version      int    `json:"version"`
}

type BookAdminResults []BookAdminObject

type PaginatedBookAdminsContent struct {
	Count   int64 `json:"count"`
	Skip    int64 `json:"skip"`
	Limit   int64 `json:"limit"`
	Results BookAdminResults
}

// NewAdmin will return an Admin{} instance, Admin structure factory function
func NewAdmin(email, firstName, lastName, password string) (*BookAdminObject, error) {
	byteArray, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		return nil, err
	}
	return &BookAdminObject{
		Email:        email,
		FirstName:    firstName,
		LastName:     lastName,
		PasswordHash: bytes.NewBuffer(byteArray).String(),
		Version:      1,
	}, nil
}
