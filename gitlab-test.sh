#!/usr/bin/env bash
set -e

./pre-build.sh

echo "Execute UT"
CFG_FILE="$(pwd)/secrets/ci.env.yaml" go test -v ./...
