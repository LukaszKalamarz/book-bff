/*
Copyright © 2022 Artkadiusz Tułodziecki <atulodzi@gmail.com>
*/
package main

import "gitlab.com/mobica-workshops/examples/go/gorilla/book-bff/cmd"

func main() {
	cmd.Execute()
}
